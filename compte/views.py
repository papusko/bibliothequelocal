from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from .forms import LoginForm, RegisterForm


def login_user(request):
    if request.user.is_authenticated:
        return redirect("/book/")
        # request.post donnees envoyer par request
    if request.method == "POST":
        form = LoginForm(request.POST)
        # cleaned_data.get veut dire recupere des donnees qui sont correct (champ vide ou pas)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            # la fonction authentiate est utiliser pour verifier si l'utilisateur existe ou pas
            user = authenticate(username=username, password=password)
            # print(user)
            if user is not None:
                login(request, user)
                return redirect("/login")
    else:
        form = LoginForm()
    return render(request, "compte/login.html", {"form": form})


def register_user(request):
    if request.user.is_authenticated:
        return redirect("/")
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get("username")
            password1 = form.cleaned_data.get("password1")
            user = authenticate(username=username, password=password1)
            login(request, user)
            return redirect("/")
    else:
        form = RegisterForm()
    return render(request, "compte/register.html", {"form": form})
