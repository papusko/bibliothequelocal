# bibliothequeLocal

## Intallation Python

 telecharger et installer python

 ```bash
    https://www.python.org/downloads/
 ```   

## Installation

Clonner le projet

```bash
git clone https://gitlab.com/papusko/bibliothequelocal.git
```

Creer un environtemment virtuel

Systeme Unix

```bash
python -m venv monenv
source monenv/bin/activate
```

Windows

```bash
py -m venv monenv
monenv\Scripts\activate.bat
```

Installer les pre-requis:

- Python >= v.3.8
- Django == 3.2

```bash
pip install django
```

## Execution

```bash
ptyhon manage.py runserver
```

