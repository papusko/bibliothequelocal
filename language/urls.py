from django.urls import path
from .views import language_list, add_language, index, delete_language, language_detail, update_language

urlpatterns = [
    path("", language_list, name="language_list" ),
    path("add/", add_language, name="add_language" ),
    path("<int:language_id>/detail/", language_detail, name="detail_language"),
    path("<int:language_id>/delete/", delete_language, name="delete_language"),
    path("<int:language_id>/update/", update_language, name="update_language")
]
