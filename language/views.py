from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Language
from .forms import LanguageForm

# Create your views here.
def index(request):
    # return HttpResponse("<h2> Salut tout le monde </h2>")
    return render(request, "index.html")

def language_list(request):
    language = Language.objects.all().order_by("-id")
    return render(request, "language/language-list.html", {"language": language})

def add_language(request):
    if request.method =="POST":
        form = LanguageForm(request.POST)
        if form.is_valid():
            language = form.save(commit=False)
            language.save()
            return redirect("language_list")
    else:
        form = LanguageForm()
    return render(request, "language/add-language.html", {"form": form})

@login_required
def language_detail(request, language_id):
    language = get_object_or_404(Language, pk=language_id)
    return render(request, "language/language-detail.html", {"language": language})

@login_required
def update_language(request, language_id):
    language = get_object_or_404(Language, pk=language_id)
    if request.method == "POST":
        form = LanguageForm(request.POST, instance=language)
        if form.is_valid():
            language = form.save(commit=False)
            language.save()
            return redirect("language_list")
    else: 
        form = LanguageForm(instance=language)
    return render(request, "language/update-language.html", {"form": form})    


@login_required
def delete_language(request, language_id):
    language = get_object_or_404(Language, pk=language_id)
    if request.method == "POST":
        language.delete()
        return redirect ("language_list")
    return render(request, "language/delete-language.html")            