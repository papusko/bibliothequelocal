from django import forms
from .models import Language

class LanguageForm(forms.ModelForm):

    name = forms.CharField(label="nom language", widget=forms.TextInput({"class": "form-control"}) )


    class Meta:
        model = Language
        fields = ('name',)
