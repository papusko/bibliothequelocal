from django.db import models

# Create your models here.
class Genre(models.Model):
    # type de livre
    name = models.CharField(max_length=255, help_text="Entre le type le type de film (film d'action, film d'horreur)")


    def __str__(self):
        return self.name