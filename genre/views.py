from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Genre
from .forms import GenreForm

# Create your views here.
def index(request):
    # return HttpResponse("<h2> Salut tout le monde </h2>")
    return render(request, "index.html")

def genre_list(request):
    genre = Genre.objects.all().order_by("-id")
    return render(request, "genre/genre-list.html", {"genre": genre})

def add_genre(request):
    if request.method =="POST":
        form = GenreForm(request.POST)
        if form.is_valid():
            genre = form.save(commit=False)
            genre.save()
            return redirect("genre_list")
    else:
        form = GenreForm()
    return render(request, "genre/add-genre.html", {"form": form})

@login_required
def genre_detail(request, genre_id):
    genre = get_object_or_404(Genre, pk=genre_id)
    return render(request, "genre/genre-detail.html", {"genre": genre})

@login_required
def update_genre(request, genre_id):
    genre = get_object_or_404(Genre, pk=genre_id)
    if request.method == "POST":
        form = GenreForm(request.POST, instance=genre)
        if form.is_valid():
            genre = form.save(commit=False)
            genre.save()
            return redirect("genre_list")
    else: 
        form = GenreForm(instance=genre)
    return render(request, "genre/update-genre.html", {"form": form})    


@login_required
def delete_genre(request, genre_id):
    genre = get_object_or_404(Genre, pk=genre_id)
    if request.method == "POST":
        genre.delete()
        return redirect ("genre_list")
    return render(request, "genre/delete-genre.html")            