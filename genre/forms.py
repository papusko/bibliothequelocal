from django import forms
from .models import Genre

class GenreForm(forms.ModelForm):
    name = forms.CharField(label="Libelles", widget=forms.TextInput({"class": "form-control"}) )

    
    class Meta:
        model = Genre
        fields = ['name',]