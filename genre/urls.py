from django.urls import path
from .views import genre_list, add_genre, index, delete_genre, genre_detail, update_genre

urlpatterns = [
    path("", genre_list, name="genre_list" ),
    path("add/", add_genre, name="add_genre" ),
    path("<int:genre_id>/detail/", genre_detail, name="detail_genre"),
    path("<int:genre_id>/delete/", delete_genre, name="delete_genre"),
    path("<int:genre_id>/update/", update_genre, name="update_genre")
]