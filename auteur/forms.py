from django import forms
from .models import Auteur

class AuteurForm(forms.ModelForm):
    first_name = forms.CharField(label="prenom auteur", widget=forms.TextInput({"class": "form-control"}) )
    last_name = forms.CharField(label="nom auteur", widget=forms.TextInput({"class": "form-control"}) )
    date_of_birth = forms.CharField(label="date de naissance auteur", widget=forms.DateInput({"class": "form-control"}) )
    date_of_dead = forms.CharField(label="date de deces auteur", widget=forms.DateInput({"class": "form-control"}) )
    
    class Meta:
        model = Auteur
        fields = ['first_name', 'last_name', 'date_of_birth', 'date_of_dead']