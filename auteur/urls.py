from django.urls import path
from .views import add_auteur,auteur_list, delete_auteur,auteur_detail, update_auteur

urlpatterns = [
    path("add/", add_auteur, name="add_auteur"),
    path("", auteur_list, name="auteur_list"),
    path("<int:auteur_id>/detail/", auteur_detail, name="detail_auteur"),
    path("<int:auteur_id>/delete/", delete_auteur, name="delete_auteur"),
    path("<int:auteur_id>/update/", update_auteur, name="update_auteur")
]