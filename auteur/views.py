from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import  HttpResponse
from .models import Auteur
from django import forms
from .forms import AuteurForm

# Create your views here.
@login_required
def add_auteur(request):
    if request.method == "POST":
        form = AuteurForm(request.POST)
        if form.is_valid():
            auteur= form.save(commit=False)
            auteur.save()
            return redirect("auteur_list")
    else:
        form = AuteurForm()
    return render(request, "auteur/add-auteur.html",{"form": form})

@login_required
def auteur_list(request):
    auteurs =  Auteur.objects.all()
    return render(request, "auteur/auteur-list.html", {"auteurs": auteurs})

@login_required
def auteur_detail(request, auteur_id):
    auteur = get_object_or_404(Auteur, pk=auteur_id)
    return render(request, "auteur/auteur-detail.html", {"auteur": auteur})

@login_required
def update_auteur(request, auteur_id):
    auteur = get_object_or_404(Auteur, pk=auteur_id)
    if request.method == "POST":
        form = AuteurForm(request.POST, instance=auteur)
        if form.is_valid():
            auteur = form.save(commit=False)
            auteur.save()
            return redirect("auteur_list")
    else: 
        form = AuteurForm(instance=auteur)
    return render(request, "auteur/update-auteur.html", {"form": form})    


@login_required
def delete_auteur(request, auteur_id):
    auteur = get_object_or_404(Auteur, pk=auteur_id)
    if request.method == "POST":
        auteur.delete()
        return redirect ("auteur_list")
    return render(request, "auteur/delete-auteur.html")    

