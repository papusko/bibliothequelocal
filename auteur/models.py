from django.db import models

# Create your models here.
class Auteur(models.Model):
    #Model auteur 
    first_name = models.CharField(max_length=200, help_text="prenom de l'auteur" )
    last_name = models.CharField(max_length=200, help_text="nom de l'auteur")
    date_of_birth = models.DateField(null=True,blank=True)
    date_of_dead = models.DateField(null=True, blank=True)


    def __str__(self):
        return self.first_name