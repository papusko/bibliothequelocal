# Generated by Django 3.2.2 on 2021-05-08 16:44

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Auteur',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(help_text="prenom de l'auteur", max_length=200)),
                ('last_name', models.CharField(help_text="nom de l'auteur", max_length=200)),
                ('date_of_birth', models.DateField(blank=True, null=True)),
                ('date_of_dead', models.DateField(blank=True, null=True)),
            ],
        ),
    ]
