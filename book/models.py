from django.db import models
from language.models import Language
from auteur.models import Auteur
from genre.models import Genre

# Create your models here.

class Book(models.Model):
    #Livres
    titre = models.CharField(max_length=200, help_text="Entrer le titre du texte")
    resume = models.TextField(max_length=255, help_text="Resume un peu le contenu du livre" )
    auteur = models.ForeignKey(Auteur, on_delete=models.SET_NULL, null =True)
    genre = models.ForeignKey(Genre, on_delete=models.SET_NULL, null =True, help_text="Choisissez le genre de livre")
    language = models.ForeignKey(Language, on_delete=models.SET_NULL,null=True, help_text="Choisir le Language du livre")

    def __str__(self):
        return self.titre


