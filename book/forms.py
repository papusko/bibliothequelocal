from  django import forms
from .models import Book
from genre.models import Genre
from auteur.models import Auteur
from language.models import Language


class BookForm(forms.ModelForm):
    titre = forms.CharField(label="donnez le titre du livre", widget=forms.TextInput({"class": "form-control"}))
    resume = forms.CharField(label="description ", widget=forms.Textarea({"class": "form-control"}))
    auteur = forms.ModelChoiceField(queryset=Auteur.objects.all(), label="Auteur", widget=forms.Select({"class":"form-control"}) )
    genre = forms.ModelChoiceField(queryset=Genre.objects.all(), label="Genre", widget=forms.SelectMultiple({"class": "form-control"}))
    language = forms.ModelChoiceField(queryset=Language.objects.all(), label="Language", widget=forms.Select({"class": "form-control"}))

    class Meta:
        model = Book
        fields = ('titre', 'resume', 'auteur','genre', 'language',)