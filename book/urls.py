from django.urls import path
from .views import book_list, add_book, index, delete_book, book_detail, update_book

urlpatterns = [
    path("", book_list, name="book_list" ),
    path("add/", add_book, name="add_book" ),
       path("<int:book_id>/detail/", book_detail, name="detail_book"),
    path("<int:book_id>/delete/", delete_book, name="delete_book"),
    path("<int:book_id>/update/", update_book, name="update_book")
]
