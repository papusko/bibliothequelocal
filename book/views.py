from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from .models import Book
from .forms import BookForm

# Create your views here.
def index(request):
    # return HttpResponse("<h2> Salut tout le monde </h2>")
    return render(request, "index.html")

def book_list(request):
    books = Book.objects.all()
    return render(request, "book/book-list.html", {"books": books})

def add_book(request):
    if request.method =="POST":
        form = BookForm(request.POST)
        if form.is_valid():
            book = form.save(commit=False)
            book.save()
            return redirect("book_list")
    else:
        form = BookForm()
    return render(request, "book/add-book.html", {"form": form})

@login_required
def book_detail(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    return render(request, "book/book-detail.html", {"book": book})

@login_required
def update_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    if request.method == "POST":
        form = BookForm(request.POST, instance=book)
        if form.is_valid():
            book = form.save(commit=False)
            book.save()
            return redirect("book_list")
    else: 
        form = BookForm(instance=book)
    return render(request, "book/update-book.html", {"form": form})    


@login_required
def delete_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    if request.method == "POST":
        book.delete()
        return redirect ("book_list")
    return render(request, "book/delete-book.html")            